get.regression.formula <- function(dependent.variable, independent.variables, interaction.degree) {
  if (missing(interaction.degree)) {
    interaction.degree <- 1
  }
  independent.variables <- independent.variables[independent.variables != ""]
  interaction.degree <- min(length(independent.variables), interaction.degree)
  interactions.matrix <- combn(independent.variables, interaction.degree)
  independent.variables.with.interactions <- apply(interactions.matrix, 2, function(variables) { paste(variables, collapse=" * ") })
  formula.as.string <- paste(dependent.variable, "~", paste(independent.variables.with.interactions, collapse=" + "), paste="")
  formula <- as.formula(formula.as.string)
  return(formula)
}

select.variables.using.stepwise.glm <- function(df, dependent.variable, independent.variables, information.criterion, interaction.degree, direction) {
  if (missing(information.criterion)) {
    information.criterion <- "AIC"
  }
  if (missing(interaction.degree)) {
    interaction.degree <- 1
  }
  if (missing(direction)) {
    interaction.degree <- "backward"
  }
  empty.glm.fit <- glm(get.regression.formula(dependent.variable, c("1")), data=df, family=binomial(link=logit))
  full.glm.fit <- glm(get.regression.formula(dependent.variable, independent.variables, interaction.degree), data=df, family=binomial(link=logit))
  k <- switch(information.criterion,
              "AIC"=2,
              "BIC"=round(log(nrow(df))),
              2
  )
  assign("df", df, envir=.GlobalEnv)
  #stepwise.glm.fit <- step(object=empty.glm.fit, scope=list(upper=full.glm.fit), k=k, trace=0, direction="backward")
  stepwise.glm.fit <- step(object=full.glm.fit, k=k, trace=0, direction=direction)
  remove(df, envir=.GlobalEnv)
  selected.independent.variables <- tail(all.vars(formula(stepwise.glm.fit)), -1)
  return(selected.independent.variables)
}

select.significant.variable.interactions.from.regression.model <- function(model.fit, significance.level) {
  if (missing(significance.level)) {
    significance.level = 0.05
  }
  variables <- data.frame(summary(model.fit)$coef[-1,])
  significant.variable.combinations <- rownames(data.frame(variables[variables[,4] <= significance.level,]))
  all.variables <- tail(all.vars(formula(model.fit)), -1)
  all.variables <- unlist(rev(all.variables[order(nchar(all.variables), all.variables)]))
  significant.variable.interactions <- lapply(significant.variable.combinations, function(significant.variable.combination) {
    individual.variables.with.values <- unlist(strsplit(significant.variable.combination, ":"))
    individual.variables <- lapply(individual.variables.with.values, function(individual.variable.with.value) { 
      individual.variable.with.value.starts.with <- unlist(startsWith(individual.variable.with.value, all.variables)) 
      if (sum(individual.variable.with.value.starts.with) == 0) {
        return(NULL)
      } else {
        index.of.longest.variable.name <- min(which(individual.variable.with.value.starts.with == TRUE))
        longest.variable.name <- all.variables[index.of.longest.variable.name]
        return(longest.variable.name)
      }
    })
    return(individual.variables)
  })
  return(significant.variable.interactions)
}

select.variables.and.interactions.using.stepwise.glm <- function(df, dependent.variable, independent.variables, information.criterion, interaction.degree, direction) {
  selected.independent.variables <- select.variables.using.stepwise.glm(df, dependent.variable, independent.variables, information.criterion, 1, direction)
  if (length(selected.independent.variables) == 0) {
    print("Stepwise regression did not select any variables. Please try again with another set of variables or try using another information criterion.")
    return
  }
  glm.fit.with.interactions <- glm(get.regression.formula(dependent.variable, selected.independent.variables, interaction.degree), data=df, family=binomial(link=logit))
  significant.variable.interactions <- select.significant.variable.interactions.from.regression.model(glm.fit.with.interactions)
  significant.variable.interactions.string <- lapply(significant.variable.interactions, function(variables) { paste(variables, collapse=" * ") })
  selected.independent.variables.with.interactions <- c(selected.independent.variables, significant.variable.interactions.string)
  selected.independent.variables <- select.variables.using.stepwise.glm(df, dependent.variable, selected.independent.variables.with.interactions, information.criterion, 1, direction)
  return(selected.independent.variables)
}

run.stepwise.glm.with.significant.interactions <- function(df, dependent.variable, independent.variables, information.criterion, interaction.degree, direction) {
  selected.independent.variables.with.interactions <- select.variables.and.interactions.using.stepwise.glm(df, dependent.variable, independent.variables, information.criterion, interaction.degree, direction)
  glm.fit <- glm(get.regression.formula(dependent.variable, selected.independent.variables.with.interactions, 1), data=df, family=binomial(link=logit))
  return(glm.fit)
}

check.glm.GOF <- function(model.fit, HL.g, plot.figures) {
  ## Goodness of fit test  for logistic regression
  # Hosmer-Lemeshow test, Osius-Rojek test and Stukel test
  # For all 3 tests, 
  # (1) If p-value < 0.01, the model is a poor fit of the data. Reconsider a different model 
  # (2) If p-value < 0.05, the model may be a good fit. Check the HL cell count. 
  if (missing(HL.g)) {
    HL.g <- 8
  }
  if (missing(plot.figures)) {
    plot.figures <- FALSE
  }
  num.tests.passed <- 0
  # Hosmer-Lemeshow test
  HL <- HLTest(model.fit, g=HL.g)
  cell.count <- cbind(HL$observed,round(HL$expect, digits=1)) #Check for cell count < 5
  print(cell.count)
  print(HL)
  # Osius-Rojek test
  OR <- o.r.test(model.fit)
  print(OR)
  # Stukel test
  S <- stukel.test(model.fit)
  print(S)
  # Plots of influence measures vs estimated probabilities of success (TRUE)
  if (plot.figures) {
    glmInflDiag(model.fit)
  }
  if (HL$p.value < 0.01 || OR$p.value < 0.01 || S$p.value < 0.01) {
    return(FALSE)
  }
  if (HL$p.value >= 0.05) {
    num.tests.passed <- num.tests.passed + 1
  }
  if (S$p.value >= 0.05) {
    num.tests.passed <- num.tests.passed + 1
  }
  if (OR$p.value >= 0.05) {
    num.tests.passed <- num.tests.passed + 1
  }
  return(num.tests.passed == 3)
}

format.glm.GOF.tests <- function(model.fit, HL.g, do.cite, output.HL.test.results, output.OR.test.results, output.S.test.results, HL.cite.key, OS.cite.key, S.cite.key) {
  if (missing(output.HL.test.results)) {
    output.HL.test.results <- TRUE
  }
  if (missing(output.OR.test.results)) {
    output.OR.test.results <- TRUE
  }
  if (missing(output.S.test.results)) {
    output.S.test.results <- TRUE
  }
  if (missing(HL.cite.key)) {
    HL.cite.key <- "Hosmer1980"
  }
  if (missing(OS.cite.key)) {
    OS.cite.key <- "Osius1992"
  }
  if (missing(S.cite.key)) {
    S.cite.key <- "Stukel1988"
  }
  ## Goodness of fit test  for logistic regression
  # Hosmer-Lemeshow test, Osius-Rojek test and Stukel test
  # For all 3 tests, 
  # (1) If p-value < 0.01, the model is a poor fit of the data. Reconsider a different model 
  # (2) If p-value < 0.05, the model may be a good fit. Check the HL cell count. 
  if (missing(HL.g)) {
    HL.g <- 8
  }
  if (missing(do.cite)) {
    do.cite <- FALSE
  }
  # Hosmer-Lemeshow test
  HL <- HLTest(model.fit, g=HL.g)
  HL.cite <- ""
  if (do.cite) {
    HL.cite <- sprintf(" \\cite{%s}", HL.cite.key)
  }
  # Osius-Rojek test
  OR <- o.r.test(model.fit)
  OR.cite <- ""
  if (do.cite) {
    OR.cite <- sprintf(" \\cite{%s}", OS.cite.key)
  }
  # Stukel test
  S <- stukel.test(model.fit)
  S.cite <- ""
  if (do.cite) {
    S.cite <- sprintf(" \\cite{%s}", S.cite.key)
  }
  N <- nrow(model.fit$model)
  test.names <- c()
  if (output.HL.test.results) {
    test.names <- c(test.names, sprintf("Hosmer-Lemeshow%s", HL.cite))
  }
  if (output.OR.test.results) {
    test.names <- c(test.names, sprintf("Osius-Rojek%s", OR.cite))
  }
  if (output.S.test.results) {
    test.names <- c(test.names, sprintf("Stukel%s", S.cite))
  }
  num.tests <- length(test.names)
  if (num.tests > 0) {
    test.names[num.tests - 1] <- sprintf("%s and %s", test.names[num.tests - 1], test.names[num.tests])
    test.names <- head(test.names, -1)
  }
  test.names <- paste(test.names, sep="", collapse=", ")
  if (num.tests == 1) {
    test.names <- sprintf("%s test", test.names)
  } else {
    test.names <- sprintf("%s tests", test.names)
  }
  results <- c()
  if (output.HL.test.results) {
    results <- c(results, sprintf("$\\chi^2 (%s, N = %s) = %s, p %s$", HL$parameter, N, round(HL$statistic, digits=2), format.p.value(HL$p.value)))
  }
  if (output.OR.test.results) {
    results <- c(results, sprintf("$z = %s, p %s$", round(OR$z, digits=2), format.p.value(OR$p.value)))
  }
  if (output.S.test.results) {
    results <- c(results, sprintf("$\\chi^2 (%s, N = %s) = %s, p %s$", S$df, N, round(S$X2, digits=2), format.p.value(S$p.value)))
  }
  if (num.tests > 0) {
    results[num.tests - 1] <- sprintf("%s and %s", results[num.tests - 1], results[num.tests])
    results <- head(results, -1)
  }
  results <- paste(results, sep="", collapse=", ")
  formatted <- sprintf("%s, %s", test.names, results)
  return(formatted)
}
