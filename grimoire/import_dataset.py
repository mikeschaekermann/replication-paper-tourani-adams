## Python script to creat the databases from dumps

## Tests to be done to speed data restore:
## (echo "SET AUTOCOMMIT=0;";     echo "SET UNIQUE_CHECKS=0;";     echo "SET FOREIGN_KEY_CHECKS=0;"; cat irc.mysql; echo "SET FOREIGN_KEY_CHECKS=1;";     echo "SET UNIQUE_CHECKS=1;";     echo "SET AUTOCOMMIT=1;";     echo "COMMIT;"; ) | mysql -u root -p$MYSQL_ROOT_PASSWORD openstack_irc
## show variables like 'bulk%';
## SET GLOBAL bulk_insert_buffer_size = 1024 * 1024 * 512;

import os
import subprocess

dumps = {
    "openstack": [
        "mailing_lists",
        "source_code",
        "reviews",
        "tickets",
    ],
    "eclipse": [
        "mailing_lists",
        "source_code",
        "reviews",
        "tickets",
    ],
}

for project in dumps:
    print project
    for dump in dumps[project]:
        file = os.path.join(project, dump + ".mysql.7z")
        database = project + "_" + dump
        print "Processing:", database, file
        mysql_cmd = "mysql -uroot -p$MYSQL_ROOT_PASSWORD"
        sql_str = "CREATE DATABASE IF NOT EXISTS " + database
        create_cmd = mysql_cmd + ' -e "' + sql_str + '"'
        print "*** Create:", create_cmd
        subprocess.call(create_cmd, shell=True)
        uncompress_cmd = "7z x " + file + " -so"
        dump_cmd = mysql_cmd + " " + database
        dump_cmd_str = uncompress_cmd + " | " + dump_cmd
        print "*** Uncompress / recover dump:", dump_cmd_str
        subprocess.call (dump_cmd_str, shell=True)
