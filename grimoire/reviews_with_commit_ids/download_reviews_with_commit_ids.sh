mkdir -p /code/grimoire/reviews_with_commit_ids/openstack
cd /code/grimoire/reviews_with_commit_ids/openstack

bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/cinder"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack-dev/devstack"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/glance"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/heat"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/keystone"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/neutron"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/nova"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/openstack-manuals"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/swift"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$OPENSTACK_USERNAME@review.openstack.org" --gerrit-project="openstack/tempest"

mkdir -p /code/grimoire/reviews_with_commit_ids/eclipse
cd /code/grimoire/reviews_with_commit_ids/eclipse

bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$ECLIPSE_USERNAME@git.eclipse.org" --gerrit-project="linuxtools/org.eclipse.linuxtools"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$ECLIPSE_USERNAME@git.eclipse.org" --gerrit-project="jgit/jgit"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$ECLIPSE_USERNAME@git.eclipse.org" --gerrit-project="egit/egit"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$ECLIPSE_USERNAME@git.eclipse.org" --gerrit-project="cdt/org.eclipse.cdt"
bicho --db-user-out=none --db-password-out=none --db-database-out=none -b gerrit -u "$ECLIPSE_USERNAME@git.eclipse.org" --gerrit-project="scout/org.eclipse.scout.rt"