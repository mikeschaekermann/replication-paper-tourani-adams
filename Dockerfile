FROM mysql:5.7
MAINTAINER Mike Schaekermann (mikeschaekermann@gmail.com)

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm
ENV PYTHONUNBUFFERED 1

RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN echo "deb http://cran.rstudio.com/bin/linux/debian jessie-cran34/" >> /etc/apt/sources.list
# Not sure if the first public key is actually needed!
RUN apt-key adv --keyserver keys.gnupg.net --recv-key 381BA480
RUN apt-key adv --keyserver keys.gnupg.net --recv-key FCAE2A0E115C3D8A

# Not sure if packages libnlopt-dev r-cran-nloptr are actually needed!
RUN    apt-get update \
    && apt-get install -y python-pip curl p7zip p7zip-full python-numpy python-pandas git python-storm python-mysqldb libnlopt-dev r-cran-nloptr r-base r-base-dev ed texlive-xetex

RUN pip install pythonbrew && pythonbrew_install && source $HOME/.pythonbrew/etc/bashrc && pythonbrew install 2.7.3 && pythonbrew use 2.7.3

RUN pip install SQLAlchemy==1.1.15
RUN pip install pymysql==0.7.11

RUN pip install textblob==0.13.1
RUN python -m textblob.download_corpora

RUN pip install beautifulsoup4==4.3.2
RUN pip install feedparser==5.2.1
RUN pip install python-dateutil==2.2
RUN cd /code/grimoire/Bicho && python setup.py install

RUN R -e "install.packages('knitr', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('caret', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('car', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('miscTools', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('gridExtra', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('xtable', repos = 'http://cran.rstudio.com/')"
RUN R -e "install.packages('scales', repos = 'http://cran.rstudio.com/')"

RUN mkdir /code
WORKDIR /code
ADD . /code/

CMD service mysql start && python -m SimpleHTTPServer 3333

EXPOSE 3333
