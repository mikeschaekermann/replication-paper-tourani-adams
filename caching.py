import os
import pandas as pd

cache_directory = 'cache'

def get_file_path_from_cache_key(cache_key):
    file_path = './%s/%s.pkl' % (cache_directory, cache_key)
    return file_path

def get_data_frame_from_file_cache(cache_key):  
    try:
        df = pd.read_pickle(get_file_path_from_cache_key(cache_key))
        return df
    except:
        return None

def save_data_frame_to_file_cache(cache_key, df):
    if not os.path.exists(cache_directory):
        os.makedirs(cache_directory)
    df.to_pickle(get_file_path_from_cache_key(cache_key))