# Project IDs
## For issues and reviews, run the following command on databases `eclipse_issues`, `eclipse_reviews`, `openstack_issues`, `openstack_reviews` to retrieve the issue/review tracker ID for each individual project: SELECT * FROM `trackers` WHERE url LIKE '%PROJECT_NAME%'
## For source code, run the following command on databases `eclipse_source_code` and `openstack_source_code` to retrieve the repository ID for each individual project: SELECT * FROM `repositories` WHERE name LIKE '%PROJECT_NAME%'
project_id_types = ['human_readable', 'issues', 'reviews', 'source_code', 'anko', 'bicho']
project_ids = {
    # eclipse_issues: SELECT * FROM `trackers` WHERE id IN (190, 71, 126, 161, 78)
    # eclipse_reviews: SELECT * FROM `trackers` WHERE id IN (419, 294, 204, 158, 318)
    # eclipse_source_code: SELECT * FROM `repositories` WHERE id IN (330, 247, 276, 275, 420)
    "eclipse": {
        "cdt": {
            "human_readable": "cdt",
            "issues": 190,
            "reviews": 419,
            "source_code": 330,
            "anko": "cdt",
            "bicho": "cdt_org.eclipse.cdt",
            "review_id_max": 40822,
        },
        "egit": {
            "human_readable": "egit",
            "issues": 71,
            "reviews": 294,
            "source_code": 247,
            "anko": "egit",
            "bicho": "egit_egit",
            "review_id_max": 35366,
        },
        "jgit": {
            "human_readable": "jgit",
            "issues": 126,
            "reviews": 204,
            "source_code": 276,
            "anko": "jgit",
            "bicho": "jgit_jgit",
            "review_id_max": 35415,
        },
        "linuxtools": {
            "human_readable": "linuxtools",
            "issues": 161,
            "reviews": 158,
            "source_code": 275,
            "anko": "org.eclipse.linuxtools",
            "bicho": "linuxtools_org.eclipse.linuxtools",
            "review_id_max": 40609,
        },
        "scout.rt": {
            "human_readable": "scout.rt",
            "issues": 78,
            "reviews": 318,
            "source_code": 420,
            "anko": "org.eclipse.scout.rt",
            "bicho": "scout_org.eclipse.scout.rt",
            "review_id_max": 40786,
        },
    },
    # openstack_issues: SELECT * FROM `trackers` WHERE id IN (25, 13, 5, 28, 11, 10, 3, 8, 2, 14)
    # openstack_reviews: SELECT * FROM `trackers` WHERE id IN (36, 1, 39, 40, 47, 144, 50, 53, 72, 73)
    # openstack_source_code: SELECT * FROM `repositories` WHERE id IN (1, 53, 93, 48, 88, 2, 45, 102, 49, 23)
    "openstack": {
        "cinder": {
            "human_readable": "cinder",
            "issues": 25,
            "reviews": 36,
            "source_code": 1,
            "anko": "cinder",
            "bicho": "openstack_cinder",
            "review_id_max": 153379,
        },
        "devstack": {
            "human_readable": "devstack",
            "issues": 13,
            "reviews": 1,
            "source_code": 53,
            "anko": "devstack",
            "bicho": "openstack-dev_devstack",
            "review_id_max": 153397,
        },
        "glance": {
            "human_readable": "glance",
            "issues": 5,
            "reviews": 39,
            "source_code": 93,
            "anko": "glance",
            "bicho": "openstack_glance",
            "review_id_max": 153425,
        },
        "heat": {
            "human_readable": "heat",
            "issues": 28,
            "reviews": 40,
            "source_code": 48,
            "anko": "heat",
            "bicho": "openstack_heat",
            "review_id_max": 153420,
        },
        "keystone": {
            "human_readable": "keystone",
            "issues": 11,
            "reviews": 47,
            "source_code": 88,
            "anko": "keystone",
            "bicho": "openstack_keystone",
            "review_id_max": 153350,
        },
        "neutron": {
            "human_readable": "neutron",
            "issues": 10,
            "reviews": 144,
            "source_code": 2,
            "anko": "neutron",
            "bicho": "openstack_neutron",
            "review_id_max": 153422,
        },
        "nova": {
            "human_readable": "nova",
            "issues": 3,
            "reviews": 50,
            "source_code": 45,
            "anko": "nova",
            "bicho": "openstack_nova",
            "review_id_max": 153389,
        },
        "openstack-manuals": {
            "human_readable": "openstack-manuals",
            "issues": 8,
            "reviews": 53,
            "source_code": 102,
            "anko": "openstack-manuals",
            "bicho": "openstack_openstack-manuals",
            "review_id_max": 153349,
        },
        "swift": {
            "human_readable": "swift",
            "issues": 2,
            "reviews": 72,
            "source_code": 49,
            "anko": "swift",
            "bicho": "openstack_swift",
            "review_id_max": 153340,
        },
        "tempest": {
            "human_readable": "tempest",
            "issues": 14,
            "reviews": 73,
            "source_code": 23,
            "anko": "tempest",
            "bicho": "openstack_tempest",
            "review_id_max": 153428,
        },
    },
}
## As comma-separated strings
### Source code:
eclipse_project_ids_source_code = ', '.join([str(ids['source_code']) for project_name, ids in project_ids['eclipse'].iteritems()])
openstack_project_ids_source_code = ', '.join([str(ids['source_code']) for project_name, ids in project_ids['openstack'].iteritems()])
### Issues
eclipse_project_ids_issues = ', '.join([str(ids['issues']) for project_name, ids in project_ids['eclipse'].iteritems()])
openstack_project_ids_issues = ', '.join([str(ids['issues']) for project_name, ids in project_ids['openstack'].iteritems()])
### Reviews
eclipse_project_ids_reviews = ', '.join([str(ids['reviews']) for project_name, ids in project_ids['eclipse'].iteritems()])
openstack_project_ids_reviews = ', '.join([str(ids['reviews']) for project_name, ids in project_ids['openstack'].iteritems()])

# Mapping from one project ID type to another
project_id_mapping_all_directions = {}
for platform_name, platform_project_ids in project_ids.iteritems():
    project_id_mapping_all_directions[platform_name] = {}
    for from_type in project_id_types:
        project_id_mapping_all_directions[platform_name][from_type] = {}
        for to_type in project_id_types:
            if from_type == to_type: continue
            project_id_mapping_all_directions[platform_name][from_type][to_type] = {}
            for project_name, ids in platform_project_ids.iteritems():
                from_value = ids[from_type]
                to_value = ids[to_type]
                project_id_mapping_all_directions[platform_name][from_type][to_type][from_value] = to_value
