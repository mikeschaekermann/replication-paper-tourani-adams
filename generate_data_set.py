# -*- coding: utf-8 -*-

import os
import re
from collections import OrderedDict

import numpy as np
import pandas as pd
from sqlalchemy import create_engine
from textblob import TextBlob

from caching import *
from projects import *

print ""


# Database connections
mysql_connection_string = 'mysql+pymysql://root:%s@localhost/' % os.environ['MYSQL_ROOT_PASSWORD']
## Eclipse
db_eclipse_source_code = create_engine(mysql_connection_string + 'eclipse_source_code')
db_eclipse_reviews = create_engine(mysql_connection_string + 'eclipse_reviews')
db_eclipse_issues = create_engine(mysql_connection_string + 'eclipse_tickets')
## OpenStack
db_openstack_source_code = create_engine(mysql_connection_string + 'openstack_source_code')
db_openstack_reviews = create_engine(mysql_connection_string + 'openstack_reviews')
db_openstack_issues = create_engine(mysql_connection_string + 'openstack_tickets')
## Combined
db_connections = {
    'eclipse': {
        'source_code': db_eclipse_source_code,
        'reviews': db_eclipse_reviews,
        'issues': db_eclipse_issues,
    },
    'openstack': {
        'source_code': db_openstack_source_code,
        'reviews': db_openstack_reviews,
        'issues': db_openstack_issues,
    },
}


# SQL query templates
sql_query_commit_messages_with_pattern = "SELECT repository_id, rev, message FROM `scmlog` WHERE repository_id IN (%s) AND message REGEXP '%s'"
sql_query_review_or_issue_comments_with_pattern = "SELECT issues.tracker_id as tracker_id, issues.issue as issue, comments.text as text FROM `comments` JOIN `issues` ON comments.issue_id = issues.id WHERE issues.tracker_id IN (%s) AND comments.text REGEXP '%s'"
sql_query_verify_review_or_issue_ids = "SELECT tracker_id, issue FROM issues WHERE (tracker_id, issue) in (%s)"
sql_query_verify_commit_ids = "SELECT repository_id, rev FROM scmlog WHERE (repository_id, rev) in (%s)"
sql_query_range_chunk_size = 1000


# Regular expressions
## General
regex_numbers = re.compile('\d+')
regex_word_boundary_mysql = '[[:>:]]'
regex_word_boundary_regular = '\\b'
## Git commit ids
regex_git_commit_id = '[0-9a-f]{40}'
regex_git_commit_id_compiled = re.compile(regex_git_commit_id, flags=re.IGNORECASE)
## Review ids
openstack_regex_review_id_general = 'review\.openstack\.org\/(#\/)?(c\/)?[0-9]{1,6}%s'
openstack_regex_review_id_mysql = openstack_regex_review_id_general % (regex_word_boundary_mysql)
openstack_regex_review_id_regular = re.compile(openstack_regex_review_id_general % (regex_word_boundary_regular))
## Issue ids
eclipse_regex_issue_id_general = '((bug|issue)[:#\s_]*[0-9]{1,6}%s|(b=|#)[0-9]{1,6}%s)'
eclipse_regex_issue_id_mysql = eclipse_regex_issue_id_general % (regex_word_boundary_mysql, regex_word_boundary_mysql)
eclipse_regex_issue_id_regular = re.compile(eclipse_regex_issue_id_general % (regex_word_boundary_regular, regex_word_boundary_regular), flags=re.IGNORECASE)
openstack_regex_issue_id_general = '((bug|issue)[:#\s_]*[0-9]{6,7}%s|(b=|#)[0-9]{6,7}%s)'
openstack_regex_issue_id_mysql = openstack_regex_issue_id_general % (regex_word_boundary_mysql, regex_word_boundary_mysql)
openstack_regex_issue_id_regular = re.compile(openstack_regex_issue_id_general % (regex_word_boundary_regular, regex_word_boundary_regular), flags=re.IGNORECASE)
# Original regular expression to detect
# issue identifiers in commit messages,
# as cited from Tourani and Adams:
#  (bug|issue)[:#\s_]*[0-9]+
#  (b=|#)[0-9]+
#  [0-9]+\b
#  \b[0-9]+


# Directories
bicho_csv_directory = 'grimoire/reviews_with_commit_ids/'


# Eclipse Links

##  Commit <-> Issue
### Commit --> Issue
eclipse_commit_messages_with_issue_ids = get_data_frame_from_file_cache('eclipse_commit_messages_with_issue_ids')
if type(eclipse_commit_messages_with_issue_ids) == type(None):
    eclipse_commit_messages_with_issue_ids = pd.read_sql_query(sql_query_commit_messages_with_pattern % (eclipse_project_ids_source_code, eclipse_regex_issue_id_mysql), con=db_eclipse_source_code)
    eclipse_commit_messages_with_issue_ids['issue_id'] = ''
    for index, row in eclipse_commit_messages_with_issue_ids.iterrows():
        issue_id = regex_numbers.search(eclipse_regex_issue_id_regular.search(row['message']).group(0)).group(0)
        eclipse_commit_messages_with_issue_ids.set_value(index, 'issue_id', issue_id)
    eclipse_commit_messages_with_issue_ids['project'] = eclipse_commit_messages_with_issue_ids['repository_id'].map(project_id_mapping_all_directions['eclipse']['source_code']['human_readable'])
    eclipse_commit_messages_with_issue_ids['issue_tracker_id'] = eclipse_commit_messages_with_issue_ids['repository_id'].map(project_id_mapping_all_directions['eclipse']['source_code']['issues'])
    save_data_frame_to_file_cache('eclipse_commit_messages_with_issue_ids', eclipse_commit_messages_with_issue_ids)
### Commit <-- Issue
eclipse_issue_comments_with_commit_ids = get_data_frame_from_file_cache('eclipse_issue_comments_with_commit_ids')
if type(eclipse_issue_comments_with_commit_ids) == type(None):
    eclipse_issue_comments_with_commit_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (eclipse_project_ids_issues, regex_git_commit_id), con=db_eclipse_issues)
    eclipse_issue_comments_with_commit_ids['commit_id'] = ''
    for index, row in eclipse_issue_comments_with_commit_ids.iterrows():
        commit_id = regex_git_commit_id_compiled.search(row['text']).group(0).lower()
        eclipse_issue_comments_with_commit_ids.set_value(index, 'commit_id', commit_id)
    eclipse_issue_comments_with_commit_ids['project'] = eclipse_issue_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['issues']['human_readable'])
    eclipse_issue_comments_with_commit_ids['repository_id'] = eclipse_issue_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['issues']['source_code'])
    save_data_frame_to_file_cache('eclipse_issue_comments_with_commit_ids', eclipse_issue_comments_with_commit_ids)

##  Commit <-> Review
### Commit --> Review (? not clear what the regex for Eclipse review ids should be)
### Commit <-- Review (commit ids mentioned by review comments)
eclipse_review_comments_with_commit_ids = get_data_frame_from_file_cache('eclipse_review_comments_with_commit_ids')
if type(eclipse_review_comments_with_commit_ids) == type(None):
    eclipse_review_comments_with_commit_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (eclipse_project_ids_reviews, regex_git_commit_id), con=db_eclipse_reviews)
    eclipse_review_comments_with_commit_ids['commit_id'] = ''
    for index, row in eclipse_review_comments_with_commit_ids.iterrows():
        commit_id = regex_git_commit_id_compiled.search(row['text']).group(0).lower()
        eclipse_review_comments_with_commit_ids.set_value(index, 'commit_id', commit_id)
    eclipse_review_comments_with_commit_ids['project'] = eclipse_review_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['reviews']['human_readable'])
    eclipse_review_comments_with_commit_ids['repository_id'] = eclipse_review_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['reviews']['source_code'])
    save_data_frame_to_file_cache('eclipse_review_comments_with_commit_ids', eclipse_review_comments_with_commit_ids)
eclipse_reviews_with_commit_ids = get_data_frame_from_file_cache('eclipse_reviews_with_commit_ids')
if type(eclipse_reviews_with_commit_ids) == type(None):
    eclipse_reviews_with_commit_ids = []
    for project_name, ids in project_ids['eclipse'].iteritems():
        bicho_csv_file_name = ids['bicho'] + '.csv'
        bicho_csv_file_path = bicho_csv_directory + 'eclipse/' + bicho_csv_file_name
        reviews_with_commit_ids = pd.read_csv(bicho_csv_file_path)
        review_id_max = ids['review_id_max']
        reviews_with_commit_ids = reviews_with_commit_ids[reviews_with_commit_ids['review_id'] <= review_id_max]
        last_patch_set_commit_ids = reviews_with_commit_ids[['review_id', 'last_patch_set_commit_id']]
        last_patch_set_commit_ids.columns = ['issue', 'commit_id']
        gerrit_commit_ids = reviews_with_commit_ids[['review_id', 'gerrit_commit_id']]
        gerrit_commit_ids.columns = ['issue', 'commit_id']
        reviews_with_commit_ids = pd.concat([
            last_patch_set_commit_ids,
            gerrit_commit_ids,
        ])
        reviews_with_commit_ids['project'] = ids['human_readable']
        reviews_with_commit_ids['repository_id'] = ids['source_code']
        eclipse_reviews_with_commit_ids.append(reviews_with_commit_ids)
    eclipse_reviews_with_commit_ids = pd.concat(eclipse_reviews_with_commit_ids).drop_duplicates().reset_index(drop=True)
    save_data_frame_to_file_cache('eclipse_reviews_with_commit_ids', eclipse_reviews_with_commit_ids)

##  Issue  <-> Review
### Issue  --> Review (not mentioned in the paper)
### Issue  <-- Review (issue ids mentioned by review comments)
eclipse_review_comments_with_issue_ids = get_data_frame_from_file_cache('eclipse_review_comments_with_issue_ids')
if type(eclipse_review_comments_with_issue_ids) == type(None):
    eclipse_review_comments_with_issue_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (eclipse_project_ids_reviews, eclipse_regex_issue_id_mysql), con=db_eclipse_reviews)
    eclipse_review_comments_with_issue_ids['issue_id'] = ''
    for index, row in eclipse_review_comments_with_issue_ids.iterrows():
        issue_id = regex_numbers.search(eclipse_regex_issue_id_regular.search(row['text']).group(0)).group(0)
        eclipse_review_comments_with_issue_ids.set_value(index, 'issue_id', issue_id)
    eclipse_review_comments_with_issue_ids['project'] = eclipse_review_comments_with_issue_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['reviews']['human_readable'])
    eclipse_review_comments_with_issue_ids['issue_tracker_id'] = eclipse_review_comments_with_issue_ids['tracker_id'].map(project_id_mapping_all_directions['eclipse']['reviews']['issues'])
    save_data_frame_to_file_cache('eclipse_review_comments_with_issue_ids', eclipse_review_comments_with_issue_ids)


# OpenStack Links

##  Commit <-> Issue
### Commit --> Issue
openstack_commit_messages_with_issue_ids = get_data_frame_from_file_cache('openstack_commit_messages_with_issue_ids')
if type(openstack_commit_messages_with_issue_ids) == type(None):
    openstack_commit_messages_with_issue_ids = pd.read_sql_query(sql_query_commit_messages_with_pattern % (openstack_project_ids_source_code, openstack_regex_issue_id_mysql), con=db_openstack_source_code)
    openstack_commit_messages_with_issue_ids['issue_id'] = ''
    for index, row in openstack_commit_messages_with_issue_ids.iterrows():
        issue_id = regex_numbers.search(openstack_regex_issue_id_regular.search(row['message']).group(0)).group(0)
        openstack_commit_messages_with_issue_ids.set_value(index, 'issue_id', issue_id)
    openstack_commit_messages_with_issue_ids['project'] = openstack_commit_messages_with_issue_ids['repository_id'].map(project_id_mapping_all_directions['openstack']['source_code']['human_readable'])
    openstack_commit_messages_with_issue_ids['issue_tracker_id'] = openstack_commit_messages_with_issue_ids['repository_id'].map(project_id_mapping_all_directions['openstack']['source_code']['issues'])
    save_data_frame_to_file_cache('openstack_commit_messages_with_issue_ids', openstack_commit_messages_with_issue_ids)
### Commit <-- Issue
openstack_issue_comments_with_commit_ids = get_data_frame_from_file_cache('openstack_issue_comments_with_commit_ids')
if type(openstack_issue_comments_with_commit_ids) == type(None):
    openstack_issue_comments_with_commit_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (openstack_project_ids_issues, regex_git_commit_id), con=db_openstack_issues)
    openstack_issue_comments_with_commit_ids['commit_id'] = ''
    for index, row in openstack_issue_comments_with_commit_ids.iterrows():
        commit_id = regex_git_commit_id_compiled.search(row['text']).group(0).lower()
        openstack_issue_comments_with_commit_ids.set_value(index, 'commit_id', commit_id)
    openstack_issue_comments_with_commit_ids['project'] = openstack_issue_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['issues']['human_readable'])
    openstack_issue_comments_with_commit_ids['repository_id'] = openstack_issue_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['issues']['source_code'])
    save_data_frame_to_file_cache('openstack_issue_comments_with_commit_ids', openstack_issue_comments_with_commit_ids)

##  Commit <-> Review
### Commit --> Review (review ids mentioned by commit messages)
openstack_commit_messages_with_review_ids = get_data_frame_from_file_cache('openstack_commit_messages_with_review_ids')
if type(openstack_commit_messages_with_review_ids) == type(None):
    openstack_commit_messages_with_review_ids = pd.read_sql_query(sql_query_commit_messages_with_pattern % (openstack_project_ids_source_code, openstack_regex_review_id_mysql), con=db_openstack_source_code)
    openstack_commit_messages_with_review_ids['review_id'] = ''
    for index, row in openstack_commit_messages_with_review_ids.iterrows():
        review_id = regex_numbers.search(openstack_regex_review_id_regular.search(row['message']).group(0)).group(0)
        openstack_commit_messages_with_review_ids.set_value(index, 'review_id', review_id)
    openstack_commit_messages_with_review_ids['project'] = openstack_commit_messages_with_review_ids['repository_id'].map(project_id_mapping_all_directions['openstack']['source_code']['human_readable'])
    openstack_commit_messages_with_review_ids['review_tracker_id'] = openstack_commit_messages_with_review_ids['repository_id'].map(project_id_mapping_all_directions['openstack']['source_code']['reviews'])
    save_data_frame_to_file_cache('openstack_commit_messages_with_review_ids', openstack_commit_messages_with_review_ids)
### Commit <-- Review (commit ids mentioned by review comments)
openstack_review_comments_with_commit_ids = get_data_frame_from_file_cache('openstack_review_comments_with_commit_ids')
if type(openstack_review_comments_with_commit_ids) == type(None):
    openstack_review_comments_with_commit_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (openstack_project_ids_reviews, regex_git_commit_id), con=db_openstack_reviews)
    openstack_review_comments_with_commit_ids['commit_id'] = ''
    for index, row in openstack_review_comments_with_commit_ids.iterrows():
        commit_id = regex_git_commit_id_compiled.search(row['text']).group(0).lower()
        openstack_review_comments_with_commit_ids.set_value(index, 'commit_id', commit_id)
    openstack_review_comments_with_commit_ids['project'] = openstack_review_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['reviews']['human_readable'])
    openstack_review_comments_with_commit_ids['repository_id'] = openstack_review_comments_with_commit_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['reviews']['source_code'])
    save_data_frame_to_file_cache('openstack_review_comments_with_commit_ids', openstack_review_comments_with_commit_ids)
openstack_reviews_with_commit_ids = get_data_frame_from_file_cache('openstack_reviews_with_commit_ids')
if type(openstack_reviews_with_commit_ids) == type(None):
    openstack_reviews_with_commit_ids = []
    for project_name, ids in project_ids['openstack'].iteritems():
        bicho_csv_file_name = ids['bicho'] + '.csv'
        bicho_csv_file_path = bicho_csv_directory + 'openstack/' + bicho_csv_file_name
        reviews_with_commit_ids = pd.read_csv(bicho_csv_file_path)
        review_id_max = ids['review_id_max']
        reviews_with_commit_ids = reviews_with_commit_ids[reviews_with_commit_ids['review_id'] <= review_id_max]
        last_patch_set_commit_ids = reviews_with_commit_ids[['review_id', 'last_patch_set_commit_id']]
        last_patch_set_commit_ids.columns = ['issue', 'commit_id']
        gerrit_commit_ids = reviews_with_commit_ids[['review_id', 'gerrit_commit_id']]
        gerrit_commit_ids.columns = ['issue', 'commit_id']
        reviews_with_commit_ids = pd.concat([
            last_patch_set_commit_ids,
            gerrit_commit_ids,
        ])
        reviews_with_commit_ids['project'] = ids['human_readable']
        reviews_with_commit_ids['repository_id'] = ids['source_code']
        openstack_reviews_with_commit_ids.append(reviews_with_commit_ids)
    openstack_reviews_with_commit_ids = pd.concat(openstack_reviews_with_commit_ids).drop_duplicates().reset_index(drop=True)
    save_data_frame_to_file_cache('openstack_reviews_with_commit_ids', openstack_reviews_with_commit_ids)

##  Issue  <-> Review
### Issue  --> Review (not mentioned in the paper)
### Issue  <-- Review (issue ids mentioned by review comments)
openstack_review_comments_with_issue_ids = get_data_frame_from_file_cache('openstack_review_comments_with_issue_ids')
if type(openstack_review_comments_with_issue_ids) == type(None):
    openstack_review_comments_with_issue_ids = pd.read_sql_query(sql_query_review_or_issue_comments_with_pattern % (openstack_project_ids_reviews, openstack_regex_issue_id_mysql), con=db_openstack_reviews)
    openstack_review_comments_with_issue_ids['issue_id'] = ''
    for index, row in openstack_review_comments_with_issue_ids.iterrows():
        issue_id = regex_numbers.search(openstack_regex_issue_id_regular.search(row['text']).group(0)).group(0)
        openstack_review_comments_with_issue_ids.set_value(index, 'issue_id', issue_id)
    openstack_review_comments_with_issue_ids['project'] = openstack_review_comments_with_issue_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['reviews']['human_readable'])
    openstack_review_comments_with_issue_ids['issue_tracker_id'] = openstack_review_comments_with_issue_ids['tracker_id'].map(project_id_mapping_all_directions['openstack']['reviews']['issues'])
    save_data_frame_to_file_cache('openstack_review_comments_with_issue_ids', openstack_review_comments_with_issue_ids)


# Code Change Metrics (from anko tool)
code_change_metrics = get_data_frame_from_file_cache('code_change_metrics')
if type(code_change_metrics) == type(None):
    code_change_metrics_list = []
    for platform_name, platform_project_ids in project_ids.iteritems():
        for project_name, ids in platform_project_ids.iteritems():
            code_change_metrics = pd.read_csv('anko/%s/%s.csv' % (platform_name, ids['anko']))
            code_change_metrics['platform'] = platform_name
            code_change_metrics['project'] = project_name
            code_change_metrics_list.append(code_change_metrics)
    code_change_metrics = pd.concat(code_change_metrics_list)
    code_change_metrics.AUTHOR_DATE = pd.to_datetime(code_change_metrics.AUTHOR_DATE)
    code_change_metrics.COMMITTER_DATE = pd.to_datetime(code_change_metrics.COMMITTER_DATE)
    save_data_frame_to_file_cache('code_change_metrics', code_change_metrics)


# Verify Links
suffixes_merge_potential_existing = ['', '_existing']

## Eclipse
eclipse_existing_issue_ids = get_data_frame_from_file_cache('eclipse_existing_issue_ids')
if type(eclipse_existing_issue_ids) == type(None):
    eclipse_potential_issue_ids = list(set(zip(eclipse_commit_messages_with_issue_ids['issue_tracker_id'], eclipse_commit_messages_with_issue_ids['issue_id']) + zip(eclipse_review_comments_with_issue_ids['issue_tracker_id'], eclipse_review_comments_with_issue_ids['issue_id'])))
    eclipse_existing_issue_ids = pd.read_sql_query(sql_query_verify_review_or_issue_ids % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in eclipse_potential_issue_ids]), con=db_eclipse_issues)
    save_data_frame_to_file_cache('eclipse_existing_issue_ids', eclipse_existing_issue_ids)
eclipse_existing_commit_ids = get_data_frame_from_file_cache('eclipse_existing_commit_ids')
if type(eclipse_existing_commit_ids) == type(None):
    eclipse_potential_commit_ids = list(set(zip(eclipse_issue_comments_with_commit_ids['repository_id'], eclipse_issue_comments_with_commit_ids['commit_id']) + zip(eclipse_review_comments_with_commit_ids['repository_id'], eclipse_review_comments_with_commit_ids['commit_id']) + zip(eclipse_reviews_with_commit_ids['repository_id'], eclipse_reviews_with_commit_ids['commit_id'])))
    eclipse_existing_commit_ids = pd.read_sql_query(sql_query_verify_commit_ids % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in eclipse_potential_commit_ids]), con=db_eclipse_source_code)
    save_data_frame_to_file_cache('eclipse_existing_commit_ids', eclipse_existing_commit_ids)
eclipse_commit_messages_with_issue_ids = pd.merge(eclipse_commit_messages_with_issue_ids, eclipse_existing_issue_ids, left_on=['issue_tracker_id', 'issue_id'], right_on=['tracker_id', 'issue'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_issue_comments_with_commit_ids = pd.merge(eclipse_issue_comments_with_commit_ids, eclipse_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_issue_comments_with_commit_ids = pd.merge(eclipse_issue_comments_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'eclipse'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_review_comments_with_commit_ids = pd.merge(eclipse_review_comments_with_commit_ids, eclipse_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_review_comments_with_commit_ids = pd.merge(eclipse_review_comments_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'eclipse'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_reviews_with_commit_ids = pd.merge(eclipse_reviews_with_commit_ids, eclipse_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_reviews_with_commit_ids = pd.merge(eclipse_reviews_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'eclipse'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
eclipse_review_comments_with_issue_ids = pd.merge(eclipse_review_comments_with_issue_ids, eclipse_existing_issue_ids, left_on=['issue_tracker_id', 'issue_id'], right_on=['tracker_id', 'issue'], how='inner', suffixes=suffixes_merge_potential_existing)


## OpenStack
openstack_existing_issue_ids = get_data_frame_from_file_cache('openstack_existing_issue_ids')
if type(openstack_existing_issue_ids) == type(None):
    openstack_potential_issue_ids = list(set(zip(openstack_commit_messages_with_issue_ids['issue_tracker_id'], openstack_commit_messages_with_issue_ids['issue_id']) + zip(openstack_review_comments_with_issue_ids['issue_tracker_id'], openstack_review_comments_with_issue_ids['issue_id'])))
    openstack_existing_issue_ids = pd.read_sql_query(sql_query_verify_review_or_issue_ids % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in openstack_potential_issue_ids]), con=db_openstack_issues)
    save_data_frame_to_file_cache('openstack_existing_issue_ids', openstack_existing_issue_ids)
openstack_existing_commit_ids = get_data_frame_from_file_cache('openstack_existing_commit_ids')
if type(openstack_existing_commit_ids) == type(None):
    openstack_potential_commit_ids = list(set(zip(openstack_issue_comments_with_commit_ids['repository_id'], openstack_issue_comments_with_commit_ids['commit_id']) + zip(openstack_review_comments_with_commit_ids['repository_id'], openstack_review_comments_with_commit_ids['commit_id']) + zip(openstack_reviews_with_commit_ids['repository_id'], openstack_reviews_with_commit_ids['commit_id'])))
    openstack_existing_commit_ids = []
    chunk_start_index = 0
    while chunk_start_index < len(openstack_potential_commit_ids):
        openstack_potential_commit_ids_chunk = openstack_potential_commit_ids[chunk_start_index:chunk_start_index + sql_query_range_chunk_size]
        chunk_start_index += sql_query_range_chunk_size
        openstack_existing_commit_ids.append(pd.read_sql_query(sql_query_verify_commit_ids % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in openstack_potential_commit_ids_chunk]), con=db_openstack_source_code))
    chunk_start_index = 0
    openstack_existing_commit_ids = pd.concat(openstack_existing_commit_ids).reset_index(drop=True)
    save_data_frame_to_file_cache('openstack_existing_commit_ids', openstack_existing_commit_ids)
openstack_existing_review_ids = get_data_frame_from_file_cache('openstack_existing_review_ids')
if type(openstack_existing_review_ids) == type(None):
    openstack_potential_review_ids = list(set(zip(openstack_commit_messages_with_review_ids['review_tracker_id'], openstack_commit_messages_with_review_ids['review_id'])))
    openstack_existing_review_ids = pd.read_sql_query(sql_query_verify_review_or_issue_ids % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in openstack_potential_review_ids]), con=db_openstack_reviews)
    save_data_frame_to_file_cache('openstack_existing_review_ids', openstack_existing_review_ids)
openstack_commit_messages_with_issue_ids = pd.merge(openstack_commit_messages_with_issue_ids, openstack_existing_issue_ids, left_on=['issue_tracker_id', 'issue_id'], right_on=['tracker_id', 'issue'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_issue_comments_with_commit_ids = pd.merge(openstack_issue_comments_with_commit_ids, openstack_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_issue_comments_with_commit_ids = pd.merge(openstack_issue_comments_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'openstack'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_commit_messages_with_review_ids = pd.merge(openstack_commit_messages_with_review_ids, openstack_existing_review_ids, left_on=['review_tracker_id', 'review_id'], right_on=['tracker_id', 'issue'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_review_comments_with_commit_ids = pd.merge(openstack_review_comments_with_commit_ids, openstack_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_review_comments_with_commit_ids = pd.merge(openstack_review_comments_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'openstack'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_reviews_with_commit_ids = pd.merge(openstack_reviews_with_commit_ids, openstack_existing_commit_ids, left_on=['repository_id', 'commit_id'], right_on=['repository_id', 'rev'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_reviews_with_commit_ids = pd.merge(openstack_reviews_with_commit_ids, code_change_metrics.loc[code_change_metrics['platform'] == 'openstack'], left_on=['project', 'commit_id'], right_on=['project', 'HASHID'], how='inner', suffixes=suffixes_merge_potential_existing)
openstack_review_comments_with_issue_ids = pd.merge(openstack_review_comments_with_issue_ids, openstack_existing_issue_ids, left_on=['issue_tracker_id', 'issue_id'], right_on=['tracker_id', 'issue'], how='inner', suffixes=suffixes_merge_potential_existing)


# Eclipse Links

##  Commit <-> Issue
eclipse_links_commit_issue = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'eclipse'),
        ('project', eclipse_commit_messages_with_issue_ids['project']),
        ('commit_id', eclipse_commit_messages_with_issue_ids['rev']),
        ('issue_id', eclipse_commit_messages_with_issue_ids['issue_id'].astype(str)),
    ))),
    pd.DataFrame(OrderedDict((
        ('platform', 'eclipse'),
        ('project', eclipse_issue_comments_with_commit_ids['project']),
        ('commit_id', eclipse_issue_comments_with_commit_ids['commit_id']),
        ('issue_id', eclipse_issue_comments_with_commit_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Commit <-> Review
eclipse_links_commit_review = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'eclipse'),
        ('project', eclipse_review_comments_with_commit_ids['project']),
        ('commit_id', eclipse_review_comments_with_commit_ids['commit_id']),
        ('review_id', eclipse_review_comments_with_commit_ids['issue'].astype(str)),
    ))),
    pd.DataFrame(OrderedDict((
        ('platform', 'eclipse'),
        ('project', eclipse_reviews_with_commit_ids['project']),
        ('commit_id', eclipse_reviews_with_commit_ids['commit_id']),
        ('review_id', eclipse_reviews_with_commit_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Issue  <-> Review
eclipse_links_issue_review = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'eclipse'),
        ('project', eclipse_review_comments_with_issue_ids['project']),
        ('issue_id', eclipse_review_comments_with_issue_ids['issue_id'].astype(str)),
        ('review_id', eclipse_review_comments_with_issue_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Commit <-> Issue <-> Reviews
eclipse_links = pd.concat([
    pd.merge(eclipse_links_commit_issue, eclipse_links_commit_review, on=['platform', 'project', 'commit_id'], how='inner'),
    pd.merge(eclipse_links_commit_issue, eclipse_links_issue_review, on=['platform', 'project', 'issue_id'], how='inner'),
    pd.merge(eclipse_links_commit_review, eclipse_links_issue_review, on=['platform', 'project', 'review_id'], how='inner'),
]).drop_duplicates('commit_id').reset_index(drop=True)
print "# Eclipse - Number of Commit <-> Issue <-> Review Links:"
print ""
print eclipse_links.project.value_counts()
print ""

# OpenStack Links

##  Commit <-> Issue
openstack_links_commit_issue = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_commit_messages_with_issue_ids['project']),
        ('commit_id', openstack_commit_messages_with_issue_ids['rev']),
        ('issue_id', openstack_commit_messages_with_issue_ids['issue_id'].astype(str)),
    ))),
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_issue_comments_with_commit_ids['project']),
        ('commit_id', openstack_issue_comments_with_commit_ids['commit_id']),
        ('issue_id', openstack_issue_comments_with_commit_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Commit <-> Review
openstack_links_commit_review = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_commit_messages_with_review_ids['project']),
        ('commit_id', openstack_commit_messages_with_review_ids['rev']),
        ('review_id', openstack_commit_messages_with_review_ids['review_id'].astype(str)),
    ))),
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_review_comments_with_commit_ids['project']),
        ('commit_id', openstack_review_comments_with_commit_ids['commit_id']),
        ('review_id', openstack_review_comments_with_commit_ids['issue'].astype(str)),
    ))),
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_reviews_with_commit_ids['project']),
        ('commit_id', openstack_reviews_with_commit_ids['commit_id']),
        ('review_id', openstack_reviews_with_commit_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Issue  <-> Review
openstack_links_issue_review = pd.concat([
    pd.DataFrame(OrderedDict((
        ('platform', 'openstack'),
        ('project', openstack_review_comments_with_issue_ids['project']),
        ('issue_id', openstack_review_comments_with_issue_ids['issue_id'].astype(str)),
        ('review_id', openstack_review_comments_with_issue_ids['issue'].astype(str)),
    ))),
]).drop_duplicates().reset_index(drop=True)
##  Commit <-> Issue <-> Review
openstack_links = pd.concat([
    pd.merge(openstack_links_commit_issue, openstack_links_commit_review, on=['platform', 'project', 'commit_id'], how='inner'),
    pd.merge(openstack_links_commit_issue, openstack_links_issue_review, on=['platform', 'project', 'issue_id'], how='inner'),
    pd.merge(openstack_links_commit_review, openstack_links_issue_review, on=['platform', 'project', 'review_id'], how='inner'),
]).drop_duplicates('commit_id').reset_index(drop=True)
print "# OpenStack - Number of Commit <-> Issue <-> Review Links:"
print ""
print openstack_links.project.value_counts()
print ""

# Links for all projects combined
links_commit_issue = pd.concat([
    eclipse_links_commit_issue,
    openstack_links_commit_issue
])
links_commit_review = pd.concat([
    eclipse_links_commit_review,
    openstack_links_commit_review
])
links_complete = pd.concat([
    eclipse_links,
    openstack_links
])
links_complete['issue_id'] = links_complete['issue_id'].astype(str)
links_complete['review_id'] = links_complete['review_id'].astype(str)


# Compute linking statistics to reproduce Table II from original paper

## Total #commits
openstack_cutoff_date = '2015-02-08'
code_change_metrics = code_change_metrics[(code_change_metrics['platform'] == 'eclipse') | ((code_change_metrics['platform'] == 'openstack') & (code_change_metrics['COMMITTER_DATE'] <= openstack_cutoff_date))]
linking_stats = code_change_metrics[['platform', 'project', 'HASHID']].groupby(['platform', 'project']).count().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats.columns = ['Platform', 'Project', 'Total #commits']
## %defective
linking_stats_percent_defective = code_change_metrics[['platform', 'project', 'bug']].groupby(['platform', 'project']).mean().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_percent_defective.columns = ['Platform', 'Project', '%defective']
linking_stats = pd.merge(linking_stats, linking_stats_percent_defective, on=['Platform', 'Project'])
linking_stats['%defective'] = (linking_stats['%defective'] * 100).round().astype(int)

## Start Date
linking_stats_start_date = code_change_metrics[['platform', 'project', 'COMMITTER_DATE']].groupby(['platform', 'project']).min().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_start_date.columns = ['Platform', 'Project', 'Start Date']
linking_stats_start_date['Start Date'] = linking_stats_start_date['Start Date'].map(lambda d: d.date())
linking_stats = pd.merge(linking_stats, linking_stats_start_date, on=['Platform', 'Project'])
## End Date (not part of the table in the original paper)
linking_stats_end_date = code_change_metrics[['platform', 'project', 'COMMITTER_DATE']].groupby(['platform', 'project']).max().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_end_date.columns = ['Platform', 'Project', 'End Date']
linking_stats_end_date['End Date'] = linking_stats_end_date['End Date'].map(lambda d: d.date())
linking_stats = pd.merge(linking_stats, linking_stats_end_date, on=['Platform', 'Project'])

## Total #reviews and total #issues
linking_stats_num_reviews = []
linking_stats_num_issues = []
for index, row in linking_stats.iterrows():
    platform_name = row['Platform']
    project_name = row['Project']
    end_date = row['End Date']
    project_issue_tracker_id = project_id_mapping_all_directions[platform_name]['human_readable']['issues'][project_name]
    linking_stats_num_issues.append(pd.read_sql_query("SELECT COUNT(*) as total FROM issues WHERE tracker_id = %s AND submitted_on < '%s'" % (project_issue_tracker_id, end_date), con=db_connections[platform_name]['issues'])['total'][0])
    project_review_tracker_id = project_id_mapping_all_directions[platform_name]['human_readable']['reviews'][project_name]
    linking_stats_num_reviews.append(pd.read_sql_query("SELECT COUNT(*) as total FROM issues WHERE tracker_id = %s AND submitted_on < '%s'" % (project_review_tracker_id, end_date), con=db_connections[platform_name]['reviews'])['total'][0])
## Total #reviews
linking_stats['Total #reviews'] = linking_stats_num_reviews
## #commits linked to reviews
linking_stats_num_commits_linked_to_reviews = links_commit_review[['platform', 'project', 'commit_id']].groupby(['platform', 'project']).count().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_num_commits_linked_to_reviews.columns = ['Platform', 'Project', '#commits linked to reviews']
linking_stats = pd.merge(linking_stats, linking_stats_num_commits_linked_to_reviews, on=['Platform', 'Project'])
## %reviews linked
linking_stats['%reviews linked'] = (linking_stats['#commits linked to reviews'] / linking_stats['Total #reviews'] * 100).round().astype(int)
## %commits linked to reviews
linking_stats['%commits linked to reviews'] = (linking_stats['#commits linked to reviews'] / linking_stats['Total #commits'] * 100).round().astype(int)
## Total #issues
linking_stats['Total #issues'] = linking_stats_num_issues
## #commits linked to issues
linking_stats_num_commits_linked_to_issues = links_commit_issue[['platform', 'project', 'commit_id']].groupby(['platform', 'project']).count().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_num_commits_linked_to_issues.columns = ['Platform', 'Project', '#commits linked to issues']
linking_stats = pd.merge(linking_stats, linking_stats_num_commits_linked_to_issues, on=['Platform', 'Project'])
## %issues linked
linking_stats['%issues linked'] = (linking_stats['#commits linked to issues'] / linking_stats['Total #issues'] * 100).round().astype(int)
## %commits linked to issues
linking_stats['%commits linked to issues'] = (linking_stats['#commits linked to issues'] / linking_stats['Total #commits'] * 100).round().astype(int)

## #comits linked to both
linking_stats_commits_linked_both = links_complete[['platform', 'project', 'commit_id']].groupby(['platform', 'project']).count().reset_index().sort(['platform', 'project'], ascending=[False, True])
linking_stats_commits_linked_both.columns = ['Platform', 'Project', '#commits linked to both']
linking_stats = pd.merge(linking_stats, linking_stats_commits_linked_both, on=['Platform', 'Project'])
## %total commits linked to both
linking_stats['%total commits linked to both'] = (linking_stats['#commits linked to both'] / linking_stats['Total #commits'] * 100).round().astype(int)
linking_stats_file_path = 'paper/linking_stats.csv'
linking_stats.to_csv(linking_stats_file_path, index=False)



full_data_set = get_data_frame_from_file_cache('full_data_set')
if type(full_data_set) == type(None):
    full_data_set = pd.merge(links_complete, code_change_metrics, left_on=['platform', 'project', 'commit_id'], right_on=['platform', 'project', 'HASHID'])

    # Discussion Metrics

    ## Helper Functions
    def get_contributor_experience(platform, developer_name, datetime):
        experience = 0
        experience_values_before_datetime = code_change_metrics[(code_change_metrics.platform == platform) & (code_change_metrics.AUTHOR_NAME == developer_name) & (code_change_metrics.COMMITTER_DATE <= datetime)].EXP
        if len(experience_values_before_datetime):
            experience = max(experience_values_before_datetime)
        return experience

    def count_number_of_lines(text):
        lines = text.split('\n')
        number_of_lines = len(list(filter(lambda x: x.strip(), lines)))
        return number_of_lines

    def extract_sentiment_polarity(text):
        try:
            return TextBlob(str(text)).sentiment.polarity
        except:
            return 0.0

    def get_extreme_value(value_a, value_b):
        if abs(value_a) > abs(value_b):
            return value_a
        else:
            return value_b

    def get_number_of_supportive_arguments(text):
        support_indicators = [
            'because',
            'therefore',
            'after',
            'for',
            'since',
            'when',
            'assuming',
            'so',
            'accordingly',
            'thus',
            'hence',
            'then',
            'consequently',
        ]
        return len(re.findall(re.compile('\\b%s\\b' % '\\b|\\b'.join(support_indicators), flags=re.IGNORECASE), text))

    def get_number_of_conflictive_arguments(text):
        conflict_indicators = [
            'however',
            'but',
            'though',
            'except',
            'not',
            'never',
            'no',
            'whereas',
            'nonetheless',
            'yet',
            'despite',
        ]
        return len(re.findall(re.compile('\\b%s\\b' % '\\b|\\b'.join(conflict_indicators), flags=re.IGNORECASE), text))
    

    ## Reporter Experience (Focus)
    print 'Reporter Experience (Focus) - starting ...'
    issues = []
    for platform_name, platform_db_connections in db_connections.iteritems():
        platform_links = links_complete.loc[links_complete['platform'] == platform_name]
        ids = zip(platform_links['project'].map(project_id_mapping_all_directions[platform_name]['human_readable']['issues']), platform_links['issue_id'])        
        platform_issues = []
        chunk_start_index = 0
        while chunk_start_index < len(ids):
            ids_chunk = ids[chunk_start_index:chunk_start_index + sql_query_range_chunk_size]
            chunk_start_index += sql_query_range_chunk_size
            platform_issues.append(pd.read_sql_query("SELECT issues.tracker_id as tracker_id, issues.issue as issue_id, people.name as reporter_name, issues.submitted_on as submitted_on FROM issues JOIN people ON submitted_by = people.id WHERE (tracker_id, issue) IN (%s)" % ','.join(['(%s, "%s")' % (project_id, id) for project_id, id in ids_chunk]), con=platform_db_connections['issues']))
        chunk_start_index = 0
        platform_issues = pd.concat(platform_issues).reset_index(drop=True)
        platform_issues['platform'] = platform_name
        platform_issues['project'] = platform_issues['tracker_id'].map(project_id_mapping_all_directions[platform_name]['issues']['human_readable'])
        issues.append(platform_issues)
    issues = pd.concat(issues).reset_index(drop=True)
    issues['Reporter Experience (Focus)'] = issues.apply(lambda x: get_contributor_experience(x.platform, x.reporter_name, x.submitted_on), axis=1)
    issues = issues[['platform', 'project', 'issue_id', 'Reporter Experience (Focus)']]
    full_data_set = pd.merge(full_data_set, issues, on=['platform', 'project', 'issue_id'])
    print 'Reporter Experience (Focus) - completed'


    ## Reviewer Experience (Focus)
    print 'Reviewer Experience (Focus) - starting ...'
    reviews = []
    for platform_name, platform_db_connections in db_connections.iteritems():
        platform_links = links_complete.loc[links_complete['platform'] == platform_name]
        ids = zip(platform_links['project'].map(project_id_mapping_all_directions[platform_name]['human_readable']['reviews']), platform_links['review_id'])
        platform_issues = []
        chunk_start_index = 0
        while chunk_start_index < len(ids):
            ids_chunk = ids[chunk_start_index:chunk_start_index + sql_query_range_chunk_size]
            chunk_start_index += sql_query_range_chunk_size
            platform_issues.append(pd.read_sql_query("SELECT issues.tracker_id as tracker_id, issues.issue as review_id, people.name as reporter_name, issues.submitted_on as submitted_on FROM issues JOIN people ON submitted_by = people.id WHERE (tracker_id, issue) IN (%s)" % ','.join(['(%s, "%s")' % (project_id, id) for project_id, id in ids_chunk]), con=platform_db_connections['reviews']))
        chunk_start_index = 0
        platform_issues = pd.concat(platform_issues).reset_index(drop=True)
        platform_issues['platform'] = platform_name
        platform_issues['project'] = platform_issues['tracker_id'].map(project_id_mapping_all_directions[platform_name]['reviews']['human_readable'])
        reviews.append(platform_issues)
    reviews = pd.concat(reviews).reset_index(drop=True)
    reviews['Reviewer Experience (Focus)'] = reviews.apply(lambda x: get_contributor_experience(x.platform, x.reporter_name, x.submitted_on), axis=1)
    reviews = reviews[['platform', 'project', 'review_id', 'Reviewer Experience (Focus)']]
    full_data_set = pd.merge(full_data_set, reviews, on=['platform', 'project', 'review_id'])
    print 'Reviewer Experience (Focus) - completed ...'



    issue_bot_patterns = {
        'eclipse': '-inbox@eclipse.org',
        'openstack': 'openstack',
    }
    issue_comments = []
    for platform_name, platform_db_connections in db_connections.iteritems():
        platform_links = links_complete.loc[links_complete['platform'] == platform_name]
        ids = zip(platform_links['project'].map(project_id_mapping_all_directions[platform_name]['human_readable']['issues']), platform_links['issue_id'])
        platform_issue_comments = []
        chunk_start_index = 0
        while chunk_start_index < len(ids):
            ids_chunk = ids[chunk_start_index:chunk_start_index + sql_query_range_chunk_size]
            chunk_start_index += sql_query_range_chunk_size
            platform_issue_comments.append(pd.read_sql_query("SELECT issues.tracker_id as tracker_id, issues.issue as issue_id, people.name as commenter_name, comments.submitted_on as submitted_on, comments.text as text FROM issues JOIN comments ON comments.issue_id = issues.id JOIN people ON comments.submitted_by = people.id WHERE (tracker_id, issue) IN (%s) ORDER BY tracker_id, issue_id, submitted_on" % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in ids_chunk]), con=platform_db_connections['issues']))
        chunk_start_index = 0
        platform_issue_comments = pd.concat(platform_issue_comments).reset_index(drop=True)
        platform_issue_comments['platform'] = platform_name
        platform_issue_comments['project'] = platform_issue_comments['tracker_id'].map(project_id_mapping_all_directions[platform_name]['issues']['human_readable'])
        platform_issue_comments = platform_issue_comments.loc[~platform_issue_comments.commenter_name.str.contains(issue_bot_patterns[platform_name], flags=re.IGNORECASE)]
        issue_comments.append(platform_issue_comments)
    issue_comments = pd.concat(issue_comments).reset_index(drop=True)

    ## Discussion Lag Issue (Time)
    print 'Discussion Lag Issue (Time) - starting ...'
    issue_comments['Discussion Lag Issue (Time)'] = ((issue_comments.issue_id == issue_comments.issue_id.shift(1)).astype(int) * issue_comments.submitted_on.diff(1)).fillna(0) / np.timedelta64(1, 's')
    issue_discussion_lag = issue_comments[['platform', 'project', 'issue_id', 'Discussion Lag Issue (Time)']].groupby(['platform', 'project', 'issue_id']).mean().reset_index()
    full_data_set = pd.merge(full_data_set, issue_discussion_lag, on=['platform', 'project', 'issue_id'])
    print 'Discussion Lag Issue (Time) - completed'

    ## Fix Time (Time)
    print 'Fix Time (Time) - starting ...'
    fix_time = issue_comments[['platform', 'project', 'issue_id', 'Discussion Lag Issue (Time)']].groupby(['platform', 'project', 'issue_id']).sum().reset_index()
    fix_time.columns = ['platform', 'project', 'issue_id', 'Fix Time (Time)']
    full_data_set = pd.merge(full_data_set, fix_time, on=['platform', 'project', 'issue_id'])
    print 'Fix Time (Time) - completed'

    ## Commenter Experience Issue (Focus)
    print 'Commenter Experience Issue (Focus) - starting ...'
    issue_comments['Commenter Experience Issue (Focus)'] = issue_comments.apply(lambda x: get_contributor_experience(x.platform, x.commenter_name, x.submitted_on), axis=1)
    issue_commenter_experience = issue_comments[['platform', 'project', 'issue_id', 'Commenter Experience Issue (Focus)']].groupby(['platform', 'project', 'issue_id']).mean().reset_index()
    full_data_set = pd.merge(full_data_set, issue_commenter_experience, on=['platform', 'project', 'issue_id'])
    print 'Commenter Experience Issue (Focus) - completed'

    ## Num. Comments Issue (Length)
    print 'Num. Comments Issue (Length) - starting ...'
    issue_number_of_comments = issue_comments[['platform', 'project', 'issue_id', 'text']].groupby(['platform', 'project', 'issue_id']).count().reset_index()
    issue_number_of_comments.columns = ['platform', 'project', 'issue_id', 'Num. Comments Issue (Length)']
    full_data_set = pd.merge(full_data_set, issue_number_of_comments, on=['platform', 'project', 'issue_id'])
    print 'Num. Comments Issue (Length) - completed'

    ## Comment Length Issue (Length)
    print 'Comment Length Issue (Length) - starting ...'
    issue_comments['Comment Length Issue (Length)'] = issue_comments['text'].map(count_number_of_lines)
    issue_number_of_comments = issue_comments[['platform', 'project', 'issue_id', 'Comment Length Issue (Length)']].groupby(['platform', 'project', 'issue_id']).sum().reset_index()
    full_data_set = pd.merge(full_data_set, issue_number_of_comments, on=['platform', 'project', 'issue_id'])
    print 'Comment Length Issue (Length) - completed'

    ## IssueArg: Supportive Arguments (Support)
    print 'IssueArg: Supportive Arguments (Support) - starting ...'
    issue_comments['Num. Supportive Arguments (Support)'] = issue_comments['text'].map(get_number_of_supportive_arguments)
    num_supportive_arguments_total = issue_comments[['platform', 'project', 'issue_id', 'Num. Supportive Arguments (Support)']].groupby(['platform', 'project', 'issue_id']).sum().reset_index()
    num_supportive_arguments_total.columns = ['platform', 'project', 'issue_id', 'IssueArg: Num. Supportive Arguments Total (Support)']
    full_data_set = pd.merge(full_data_set, num_supportive_arguments_total, on=['platform', 'project', 'issue_id'])
    num_supportive_arguments_avg = issue_comments[['platform', 'project', 'issue_id', 'Num. Supportive Arguments (Support)']].groupby(['platform', 'project', 'issue_id']).mean().reset_index()
    num_supportive_arguments_avg.columns = ['platform', 'project', 'issue_id', 'IssueArg: Num. Supportive Arguments Avg (Support)']
    full_data_set = pd.merge(full_data_set, num_supportive_arguments_avg, on=['platform', 'project', 'issue_id'])
    print 'IssueArg: Supportive Arguments (Support) - completed'

    ## IssueArg: Conflictive Arguments (Conflict)
    print 'IssueArg: Conflictive Arguments (Conflict) - starting ...'
    issue_comments['Num. Conflictive Arguments (Conflict)'] = issue_comments['text'].map(get_number_of_conflictive_arguments)
    num_conflictive_arguments_total = issue_comments[['platform', 'project', 'issue_id', 'Num. Conflictive Arguments (Conflict)']].groupby(['platform', 'project', 'issue_id']).sum().reset_index()
    num_conflictive_arguments_total.columns = ['platform', 'project', 'issue_id', 'IssueArg: Num. Conflictive Arguments Total (Conflict)']
    full_data_set = pd.merge(full_data_set, num_conflictive_arguments_total, on=['platform', 'project', 'issue_id'])
    num_conflictive_arguments_avg = issue_comments[['platform', 'project', 'issue_id', 'Num. Conflictive Arguments (Conflict)']].groupby(['platform', 'project', 'issue_id']).mean().reset_index()
    num_conflictive_arguments_avg.columns = ['platform', 'project', 'issue_id', 'IssueArg: Num. Conflictive Arguments Avg (Conflict)']
    full_data_set = pd.merge(full_data_set, num_conflictive_arguments_avg, on=['platform', 'project', 'issue_id'])
    print 'IssueArg: Conflictive Arguments (Conflict) - completed'

    ## Sentiment
    print 'Sentiment - starting ...'
    # issue_comment_paragraphs = issue_comments['text'].str.split('\n\n').apply(pd.Series, 1).stack()
    # issue_comment_paragraphs.index = issue_comment_paragraphs.index.droplevel(-1)
    # issue_comment_paragraphs = issue_comment_paragraphs.str.split('\r\n\r\n').apply(pd.Series, 1).stack()
    # issue_comment_paragraphs.index = issue_comment_paragraphs.index.droplevel(-1)
    # issue_comment_paragraphs.name = 'paragraphs'
    # issue_comment_paragraphs = issue_comment_paragraphs[issue_comment_paragraphs.str.strip() != '']
    # del issue_comments['text']
    # issue_comments = issue_comments.join(issue_comment_paragraphs)
    # issue_comments['Comment Sentiment Issue (Sentiment)'] = issue_comments['paragraphs'].map(extract_sentiment_polarity)
    issue_comments['Comment Sentiment Issue (Sentiment)'] = issue_comments['text'].map(extract_sentiment_polarity)
    print 'Sentiment - completed'

    ## Sentiment Min Issue (Sentiment)
    print 'Sentiment Min Issue (Sentiment) - starting ...'
    issue_sentiment_min = issue_comments[['platform', 'project', 'issue_id', 'Comment Sentiment Issue (Sentiment)']].groupby(['platform', 'project', 'issue_id']).min().reset_index()
    issue_sentiment_min.columns = ['platform', 'project', 'issue_id', 'Sentiment Min Issue (Sentiment)']
    full_data_set = pd.merge(full_data_set, issue_sentiment_min, on=['platform', 'project', 'issue_id'])
    print 'Sentiment Min Issue (Sentiment) - completed'

    ## Sentiment Max Issue (Sentiment)
    print 'Sentiment Max Issue (Sentiment) - starting ...'
    issue_sentiment_max = issue_comments[['platform', 'project', 'issue_id', 'Comment Sentiment Issue (Sentiment)']].groupby(['platform', 'project', 'issue_id']).max().reset_index()
    issue_sentiment_max.columns = ['platform', 'project', 'issue_id', 'Sentiment Max Issue (Sentiment)']
    full_data_set = pd.merge(full_data_set, issue_sentiment_max, on=['platform', 'project', 'issue_id'])
    print 'Sentiment Max Issue (Sentiment) - completed'

    ## Sentiment Extreme Issue (Sentiment)
    print 'Sentiment Extreme Issue (Sentiment) - starting ...'
    full_data_set['Sentiment Extreme Issue (Sentiment)'] = full_data_set.apply(lambda x: get_extreme_value(x['Sentiment Min Issue (Sentiment)'], x['Sentiment Max Issue (Sentiment)']), axis=1)
    print 'Sentiment Extreme Issue (Sentiment) - completed'

    ## Sentiment Avg Issue (Sentiment)
    print 'Sentiment Avg Issue (Sentiment) - starting ...'
    issue_sentiment_avg = issue_comments[['platform', 'project', 'issue_id', 'Comment Sentiment Issue (Sentiment)']].groupby(['platform', 'project', 'issue_id']).mean().reset_index()
    issue_sentiment_avg.columns = ['platform', 'project', 'issue_id', 'Sentiment Avg Issue (Sentiment)']
    full_data_set = pd.merge(full_data_set, issue_sentiment_avg, on=['platform', 'project', 'issue_id'])
    print 'Sentiment Avg Issue (Sentiment) - completed'




    review_bot_patterns = {
        'eclipse': [
            'Hudson CI',
            'Gerrit Code Review @ Eclipse.org',
        ],
        'openstack': [
            'Red Hat CI',
            'Citrix XenServer CI',
            'Jenkins',
            'OpenStack Proposal Bot',
            'Trivial Rebase',
            'THSTACK CI',
            'Gerrit Code Review',
            'OpenDaylight CI',
            'Nimble Storage CI',
            'Coraid CI',
            'NetApp CI',
            'Mellanox CI',
            'HDS HNAS CI',''
            'Dell StorageCenter CI',
            'Scality CI',
            'Snabb NFV CI',
            'NEC CI',
            'A10 Networks CI',
            'Datera CI',
            'VMware NSX CI',
            'Embrane CI',
            'IBM GPFS CI',
            'Metaplugin CI',
            'VMWare Congress CI',
            'IBM SDN-VE CI',
            'HP Storage CI',
            'IBM Storwize CI',
            'IBM xCAT CI',
            'IBM PowerVC CI',
            'Pure Storage CI',
        ],
    }
    review_comments = []
    for platform_name, platform_db_connections in db_connections.iteritems():
        platform_links = links_complete.loc[links_complete['platform'] == platform_name]
        ids = zip(platform_links['project'].map(project_id_mapping_all_directions[platform_name]['human_readable']['reviews']), platform_links['review_id'].astype(int))
        platform_review_comments = []
        chunk_start_index = 0
        while chunk_start_index < len(ids):
            ids_chunk = ids[chunk_start_index:chunk_start_index + sql_query_range_chunk_size]
            chunk_start_index += sql_query_range_chunk_size
            platform_review_comments.append(pd.read_sql_query("SELECT issues.tracker_id as tracker_id, issues.issue as review_id, people.name as commenter_name, comments.submitted_on as submitted_on, comments.text as text FROM issues JOIN comments ON comments.issue_id = issues.id JOIN people ON comments.submitted_by = people.id WHERE (tracker_id, issue) IN (%s) ORDER BY tracker_id, issue_id, submitted_on" % ','.join(['(%s,"%s")' % (project_id, id) for project_id, id in ids_chunk]), con=platform_db_connections['reviews']))
        chunk_start_index = 0
        platform_review_comments = pd.concat(platform_review_comments).reset_index(drop=True)
        platform_review_comments['platform'] = platform_name
        platform_review_comments['project'] = platform_review_comments['tracker_id'].map(project_id_mapping_all_directions[platform_name]['reviews']['human_readable'])
        platform_review_comments = platform_review_comments.loc[~platform_review_comments.commenter_name.isin(review_bot_patterns[platform_name])]
        review_comments.append(platform_review_comments)
    review_comments = pd.concat(review_comments).reset_index(drop=True)

    ## Num. Patch Revisions (Focus)
    print 'Num. Patch Revisions (Focus) - starting ...'
    def is_comment_for_new_patch_revision(text):
        return int(text.startswith('Uploaded patch set '))
    review_comments['Num. Patch Revisions (Focus)'] = review_comments['text'].map(is_comment_for_new_patch_revision)
    review_number_of_patch_revisions = review_comments[['platform', 'project', 'review_id', 'Num. Patch Revisions (Focus)']].groupby(['platform', 'project', 'review_id']).sum().reset_index()
    full_data_set = pd.merge(full_data_set, review_number_of_patch_revisions, on=['platform', 'project', 'review_id'])
    review_comments = review_comments.loc[review_comments['Num. Patch Revisions (Focus)'] == 0]
    print 'Num. Patch Revisions (Focus) - completed'

    ## Discussion Lag Review (Time)
    print 'Discussion Lag Review (Time) - starting ...'
    review_comments['Discussion Lag Review (Time)'] = ((review_comments.review_id == review_comments.review_id.shift(1)).astype(int) * review_comments.submitted_on.diff(1)).fillna(0) / np.timedelta64(1, 's')
    review_discussion_lag = review_comments[['platform', 'project', 'review_id', 'Discussion Lag Review (Time)']].groupby(['platform', 'project', 'review_id']).mean().reset_index()
    full_data_set = pd.merge(full_data_set, review_discussion_lag, on=['platform', 'project', 'review_id'])
    print 'Discussion Lag Review (Time) - completed'

    ## Review Time (Time)
    print 'Review Time (Time) - starting ...'
    review_time = review_comments[['platform', 'project', 'review_id', 'Discussion Lag Review (Time)']].groupby(['platform', 'project', 'review_id']).sum().reset_index()
    review_time.columns = ['platform', 'project', 'review_id', 'Review Time (Time)']
    full_data_set = pd.merge(full_data_set, review_time, on=['platform', 'project', 'review_id'])
    print 'Review Time (Time) - completed'

    ## Commenter Experience Review (Focus)
    print 'Commenter Experience Review (Focus) - starting ...'
    review_comments['Commenter Experience Review (Focus)'] = review_comments.apply(lambda x: get_contributor_experience(x.platform, x.commenter_name, x.submitted_on), axis=1)
    review_commenter_experience = review_comments[['platform', 'project', 'review_id', 'Commenter Experience Review (Focus)']].groupby(['platform', 'project', 'review_id']).mean().reset_index()
    full_data_set = pd.merge(full_data_set, review_commenter_experience, on=['platform', 'project', 'review_id'])
    print 'Commenter Experience Review (Focus) - completed'

    ## Num. Comments Review (Length)
    print 'Num. Comments Review (Length) - starting ...'
    review_number_of_comments = review_comments[['platform', 'project', 'review_id', 'text']].groupby(['platform', 'project', 'review_id']).count().reset_index()
    review_number_of_comments.columns = ['platform', 'project', 'review_id', 'Num. Comments Review (Length)']
    full_data_set = pd.merge(full_data_set, review_number_of_comments, on=['platform', 'project', 'review_id'])
    print 'Num. Comments Review (Length) - completed'

    ## Comment Length Review (Length)
    print 'Comment Length Review (Length) - starting ...'
    review_comments['Comment Length Review (Length)'] = review_comments['text'].map(count_number_of_lines)
    review_number_of_comments = review_comments[['platform', 'project', 'review_id', 'Comment Length Review (Length)']].groupby(['platform', 'project', 'review_id']).sum().reset_index()
    full_data_set = pd.merge(full_data_set, review_number_of_comments, on=['platform', 'project', 'review_id'])
    print 'Comment Length Review (Length) - completed'

    ## ReviewArg: Supportive Arguments (Support)
    print 'ReviewArg: Supportive Arguments (Support) - starting ...'
    review_comments['Num. Supportive Arguments (Support)'] = review_comments['text'].map(get_number_of_supportive_arguments)
    num_supportive_arguments_total = review_comments[['platform', 'project', 'review_id', 'Num. Supportive Arguments (Support)']].groupby(['platform', 'project', 'review_id']).sum().reset_index()
    num_supportive_arguments_total.columns = ['platform', 'project', 'review_id', 'ReviewArg: Num. Supportive Arguments Total (Support)']
    full_data_set = pd.merge(full_data_set, num_supportive_arguments_total, on=['platform', 'project', 'review_id'])
    num_supportive_arguments_avg = review_comments[['platform', 'project', 'review_id', 'Num. Supportive Arguments (Support)']].groupby(['platform', 'project', 'review_id']).mean().reset_index()
    num_supportive_arguments_avg.columns = ['platform', 'project', 'review_id', 'ReviewArg: Num. Supportive Arguments Avg (Support)']
    full_data_set = pd.merge(full_data_set, num_supportive_arguments_avg, on=['platform', 'project', 'review_id'])
    print 'ReviewArg: Supportive Arguments (Support) - completed'

    ## ReviewArg: Conflictive Arguments (Conflict)
    print 'ReviewArg: Conflictive Arguments (Conflict) - starting ...'
    review_comments['Num. Conflictive Arguments (Conflict)'] = review_comments['text'].map(get_number_of_conflictive_arguments)
    num_conflictive_arguments_total = review_comments[['platform', 'project', 'review_id', 'Num. Conflictive Arguments (Conflict)']].groupby(['platform', 'project', 'review_id']).sum().reset_index()
    num_conflictive_arguments_total.columns = ['platform', 'project', 'review_id', 'ReviewArg: Num. Conflictive Arguments Total (Conflict)']
    full_data_set = pd.merge(full_data_set, num_conflictive_arguments_total, on=['platform', 'project', 'review_id'])
    num_conflictive_arguments_avg = review_comments[['platform', 'project', 'review_id', 'Num. Conflictive Arguments (Conflict)']].groupby(['platform', 'project', 'review_id']).mean().reset_index()
    num_conflictive_arguments_avg.columns = ['platform', 'project', 'review_id', 'ReviewArg: Num. Conflictive Arguments Avg (Conflict)']
    full_data_set = pd.merge(full_data_set, num_conflictive_arguments_avg, on=['platform', 'project', 'review_id'])
    print 'ReviewArg: Conflictive Arguments (Conflict) - completed'

    ## Sentiment
    print 'Sentiment - starting ...'
    # review_comment_paragraphs = review_comments['text'].str.split('\n\n').apply(pd.Series, 1).stack()
    # review_comment_paragraphs.index = review_comment_paragraphs.index.droplevel(-1)
    # review_comment_paragraphs = review_comment_paragraphs.str.split('\r\n\r\n').apply(pd.Series, 1).stack()
    # review_comment_paragraphs.index = review_comment_paragraphs.index.droplevel(-1)
    # review_comment_paragraphs.name = 'paragraphs'
    # review_comment_paragraphs = review_comment_paragraphs[review_comment_paragraphs.str.strip() != '']
    # del review_comments['text']
    # review_comments = review_comments.join(review_comment_paragraphs)
    # review_comments['Comment Sentiment Review (Sentiment)'] = review_comments['paragraphs'].map(extract_sentiment_polarity)
    review_comments['Comment Sentiment Review (Sentiment)'] = review_comments['text'].map(extract_sentiment_polarity)
    print 'Sentiment - completed'

    ## Sentiment Min Review (Sentiment)
    print 'Sentiment Min Review (Sentiment) - starting ...'
    review_sentiment_min = review_comments[['platform', 'project', 'review_id', 'Comment Sentiment Review (Sentiment)']].groupby(['platform', 'project', 'review_id']).min().reset_index()
    review_sentiment_min.columns = ['platform', 'project', 'review_id', 'Sentiment Min Review (Sentiment)']
    full_data_set = pd.merge(full_data_set, review_sentiment_min, on=['platform', 'project', 'review_id'])
    print 'Sentiment Min Review (Sentiment) - completed'

    ## Sentiment Max Review (Sentiment)
    print 'Sentiment Max Review (Sentiment) - starting ...'
    review_sentiment_max = review_comments[['platform', 'project', 'review_id', 'Comment Sentiment Review (Sentiment)']].groupby(['platform', 'project', 'review_id']).max().reset_index()
    review_sentiment_max.columns = ['platform', 'project', 'review_id', 'Sentiment Max Review (Sentiment)']
    full_data_set = pd.merge(full_data_set, review_sentiment_max, on=['platform', 'project', 'review_id'])
    print 'Sentiment Max Review (Sentiment) - completed'

    ## Sentiment Extreme Review (Sentiment)
    print 'Sentiment Extreme Review (Sentiment) - starting ...'
    full_data_set['Sentiment Extreme Review (Sentiment)'] = full_data_set.apply(lambda x: get_extreme_value(x['Sentiment Min Review (Sentiment)'], x['Sentiment Max Review (Sentiment)']), axis=1)
    print 'Sentiment Extreme Review (Sentiment) - completed'

    ## Sentiment Avg Review (Sentiment)
    print 'Sentiment Avg Review (Sentiment) - starting ...'
    review_sentiment_avg = review_comments[['platform', 'project', 'review_id', 'Comment Sentiment Review (Sentiment)']].groupby(['platform', 'project', 'review_id']).mean().reset_index()
    review_sentiment_avg.columns = ['platform', 'project', 'review_id', 'Sentiment Avg Review (Sentiment)']
    full_data_set = pd.merge(full_data_set, review_sentiment_avg, on=['platform', 'project', 'review_id'])
    print 'Sentiment Avg Review (Sentiment) - completed'

    save_data_frame_to_file_cache('full_data_set', full_data_set)

# Polishing and output of the final data set
print 'Output Final Data Set - starting ...'
final_data_set_final_output_selection = OrderedDict((
    # Software Project
    ('platform', 'Platform'),
    ('project', 'Project'),
    
    # Triple: Commit <-> Issue <-> Review
    ('commit_id', 'Commit'),
    ('issue_id', 'Issue'),
    ('review_id', 'Review'),

    # Code Change Metrics
    ## Diffusion
    ('NS', 'Code Change: NS (Diffusion)'),
    ('ND', 'Code Change: ND (Diffusion)'),
    ('NF', 'Code Change: NF (Diffusion)'),
    ('Ent', 'Code Change: ENT (Diffusion)'),
    ## Size
    ('ADD', 'Code Change: LA (Size)'),
    ('DEL', 'Code Change: LD (Size)'),
    ('LT', 'Code Change: LT (Size)'),
    ## Purpose
    ('CHANGE_TYPE', 'Code Change: FIX (Purpose)'),
    ## History
    ('NDEV', 'Code Change: NDEV (History)'),
    ('AGE', 'Code Change: AGE (History)'),
    ('NFC', 'Code Change: NUC (History)'),
    ## Experience
    ('EXP', 'Code Change: EXP (Experience)'),
    ('REXP', 'Code Change: REXP (Experience)'),
    ('SEXP', 'Code Change: SEXP (Experience)'),

    # Discussion Metrics
    ## Focus
    ('Commenter Experience Issue (Focus)', 'Issue: Commenter Experience (Focus)'),
    ('Commenter Experience Review (Focus)', 'Review: Commenter Experience (Focus)'),
    ('Reporter Experience (Focus)', 'Issue: Reporter Experience (Focus)'),
    ('Reviewer Experience (Focus)', 'Review: Reviewer Experience (Focus)'),
    ('Num. Patch Revisions (Focus)', 'Review: Num Patch Revisions (Focus)'),
    ## Length
    ('Num. Comments Issue (Length)', 'Issue: Num Comments (Length)'),
    ('Num. Comments Review (Length)', 'Review: Num Comments (Length)'),
    ('Comment Length Issue (Length)', 'Issue: Comment Length (Length)'),
    ('Comment Length Review (Length)', 'Review: Comment Length (Length)'),
    ## Time
    ('Review Time (Time)', 'Review: Review Time (Time)'),
    ('Fix Time (Time)', 'Issue: Fix Time (Time)'),
    ('Discussion Lag Issue (Time)', 'Issue: Discussion Lag (Time)'),
    ('Discussion Lag Review (Time)', 'Review: Discussion Lag (Time)'),
    ## Sentiment
    ('Sentiment Min Issue (Sentiment)', 'Issue: Sentiment Min (Sentiment)'),
    ('Sentiment Min Review (Sentiment)', 'Review: Sentiment Min (Sentiment)'),
    ('Sentiment Max Issue (Sentiment)', 'Issue: Sentiment Max (Sentiment)'),
    ('Sentiment Max Review (Sentiment)', 'Review: Sentiment Max (Sentiment)'),
    ('Sentiment Extreme Issue (Sentiment)', 'Issue: Sentiment Extreme (Sentiment)'),
    ('Sentiment Extreme Review (Sentiment)', 'Review: Sentiment Extreme (Sentiment)'),
    ('Sentiment Avg Issue (Sentiment)', 'Issue: Sentiment Avg (Sentiment)'),
    ('Sentiment Avg Review (Sentiment)', 'Review: Sentiment Avg (Sentiment)'),

    # Argumentation Metrics
    ## Support
    ('IssueArg: Num. Supportive Arguments Avg (Support)', 'IssueArg: Num. Supportive Arguments Avg (Support)'),
    ('ReviewArg: Num. Supportive Arguments Avg (Support)', 'ReviewArg: Num. Supportive Arguments Avg (Support)'),
    ('IssueArg: Num. Supportive Arguments Total (Support)', 'IssueArg: Num. Supportive Arguments Total (Support)'),
    ('ReviewArg: Num. Supportive Arguments Total (Support)', 'ReviewArg: Num. Supportive Arguments Total (Support)'),
    ## Conflict
    ('IssueArg: Num. Conflictive Arguments Avg (Conflict)', 'IssueArg: Num. Conflictive Arguments Avg (Conflict)'),
    ('ReviewArg: Num. Conflictive Arguments Avg (Conflict)', 'ReviewArg: Num. Conflictive Arguments Avg (Conflict)'),
    ('IssueArg: Num. Conflictive Arguments Total (Conflict)', 'IssueArg: Num. Conflictive Arguments Total (Conflict)'),
    ('ReviewArg: Num. Conflictive Arguments Total (Conflict)', 'ReviewArg: Num. Conflictive Arguments Total (Conflict)'),

    # Bug?
    ('bug', 'Bug'),
))
final_data_set = full_data_set[final_data_set_final_output_selection.keys()]
final_data_set.columns = final_data_set_final_output_selection.values()
final_data_set = final_data_set.sort(['Platform', 'Project'], ascending=[True, True])
final_data_set_file_path = 'paper/final_data_set.csv'
final_data_set.to_csv(final_data_set_file_path, index=False)
print 'Output Final Data Set - completed'
print ''
print "The final data set with all code-change"
print "and discussion metrics was stored in:"
print ""
print "    %s" % final_data_set_file_path
print ""
print "Linking statistics were stored in:"
print ""
print "    %s" % linking_stats_file_path
print ""
